// import { toKebabCase } from "../lib/utils";
const utils = require('../lib/utils');

const { toKebabCase } = utils;

describe('String kebab-case parser', () => {
  /* General validation */
  it('only allows lowercase letters, numbers and dashes', () => {
    const str = 'section TiTle bAdTyped>>(extra)  99hi10 soeo "';
    expect(toKebabCase(str)).toMatch(/[a-z0-9-]+/);
  });

  it('correctly parses regular string', () => {
    const str = 'Section Title Easy';
    expect(toKebabCase(str)).toBe('section-title-easy');
  });

  it('correctly parses string with special characters', () => {
    const str = 'Section Title,        !NotEasy#! ""()';
    expect(toKebabCase(str)).toBe('section-title-not-easy');
  });

  it('correctly parses string with weird capitalization and special characters', () => {
    const str = 'section TiTle bAdTyped>>(extra)  99hi10';
    expect(toKebabCase(str)).toBe('section-ti-tle-b-ad-typed-extra-99-hi10');
  });
});
